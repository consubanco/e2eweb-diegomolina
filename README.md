## Tecnología ##
Java 17
Gradle 8.7

## Ejecutar ##
1.- En el archivo src/test/java/runners/Runner registrar el tag de la prueba "Feature" que se va a ejecutar
2.- Clic derecho en el archivo Runner y ejecutar "Run 'Runner'"

## REPORTE ##
El reporte se puede obtener en la ruta **target/site/serenity/index.html**