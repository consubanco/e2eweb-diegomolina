package com.pruebas.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

import static com.pruebas.ui.PaginaPrincipal.*;

public class ListarPhones implements Task {

    public static ListarPhones visualizar(){
        return Tasks.instrumented(ListarPhones.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(SELECT_TYPE_PRODUCT)
        );
    }
}
