package com.pruebas.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static com.pruebas.ui.PaginaPrincipal.*;


public class LoginWeb implements Task {

    private final String user;
    private final String pass;

    public LoginWeb(String user, String pass){
        this.user = user;
        this.pass = pass;
    }

    public static LoginWeb conLasCredenciales(String user, String pass){
        return Tasks.instrumented(LoginWeb.class, user, pass);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(BUTTON_LOGIN),
                Enter.theValue(user).into(TXT_LOGIN_USUARIO),
                Enter.theValue(pass).into(TXT_LOGIN_PASS),
                Click.on(BUTTON_INGRESAR)
        );
    }
}
