package com.pruebas.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static com.pruebas.ui.PaginaPrincipal.*;


public class RegistrarWeb implements Task {

    private final String user;
    private final String pass;

    public RegistrarWeb(String user, String pass){
        this.user = user;
        this.pass = pass;
    }

    public static RegistrarWeb conLasCredenciales(String user, String pass){
        return Tasks.instrumented(RegistrarWeb.class, user, pass);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(BUTTON_REGISTRE),
                Enter.theValue(user).into(TXT_USER),
                Enter.theValue(pass).into(TXT_PASS),
                Click.on(BUTTON_REGISTREUSER)
        );
    }
}
