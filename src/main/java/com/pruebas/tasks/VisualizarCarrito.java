package com.pruebas.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

import static com.pruebas.ui.UserInterfaceCarrito.BUTTON_COMPRAS;

public class VisualizarCarrito implements Task {

    public static VisualizarCarrito producto(){
        return Tasks.instrumented(VisualizarCarrito.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(BUTTON_COMPRAS)
        );
    }
}
