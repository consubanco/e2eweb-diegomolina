package com.pruebas.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

import static com.pruebas.ui.PaginaProductos.*;

public class VerDescripcion implements Task {

    public static VerDescripcion telefonos(){
        return Tasks.instrumented(VerDescripcion.class);
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(LABEL_SAMSUNG)
        );
    }
}
