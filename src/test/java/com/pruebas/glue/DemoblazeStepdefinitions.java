package com.pruebas.glue;

import com.pruebas.model.InformacionCliente;
import com.pruebas.tasks.*;
import com.pruebas.ui.CarritoCompra;
import com.pruebas.ui.PaginaPrincipal;
import com.pruebas.ui.PaginaProductos;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.actions.Open;
import com.pruebas.questions.QuesGetText;

import static org.hamcrest.CoreMatchers.containsString;

import static com.pruebas.ui.CarritoCompra.SUCCESSFULL_PURCHASE;
import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isPresent;
import static net.serenitybdd.screenplay.questions.WebElementQuestion.the;

public class DemoblazeStepdefinitions {

    @Given("que el {actor} ingresa a la pagina de demoblaze para la compra de telefonos selecciona el {string}")
    public void quiereComprarProducto(Actor actor, String descripcion) {
        givenThat(actor).attemptsTo(Open.browserOn().the(PaginaPrincipal.class));
        andThat(actor).wasAbleTo(
                BuscarProducto.conDescripcion(descripcion),
                AnadirProducto.alCarrito(descripcion));
    }

    @When("el decide hacer la compra ingresa sus datos personales {string}, {string}, {string}, {string}, {string} y {string}")
    public void ySeIdentificaConLosDatosDeCompraY(String nombre, String pais, String ciudad, String numeroDeTarjeta,
            String mesVencimiento, String anioVencimiento) {
        when(theActorInTheSpotlight()).wasAbleTo(
                RegistrarCliente.conInformacionCompra(InformacionCliente.conDatos(nombre, pais, ciudad, numeroDeTarjeta,
                        mesVencimiento, anioVencimiento)));
    }

    @Then("el realiza la compra del producto exitosamente")
    public void completaLaCompraExitosamenteDelProducto() {
        then(theActorInTheSpotlight()).should(seeThat(the(SUCCESSFULL_PURCHASE), isPresent()));
    }

    @Given("que el {actor} ingresa a la pagina de demoblaze para  registrase")
    public void queElClienteIngresaALaPaginaDeDemoblazeParaRegistrase(Actor actor) {
        givenThat(actor).attemptsTo(Open.browserOn().the(PaginaPrincipal.class));
    }

    @When("ingresa a la opcion sigup e ingresa {string} y {string}")
    public void ingresaALaOpcionSigupEIngresaY(String usuario, String pass) {
        when(theActorInTheSpotlight()).wasAbleTo(
                RegistrarWeb.conLasCredenciales(usuario,pass)
        );
    }

    @Then("el sistema le debe dejar registrase correctamente")
    public void elSistemaLeDebeDejarRegistraseCorrectamente() {
    }

    @Given("que el {actor} ingresa a la pagina de demoblaze para  comprar")
    public void queElClienteIngresaALaPaginaDeDemoblazeParaComprar(Actor actor) {
        givenThat(actor).attemptsTo(Open.browserOn().the(PaginaPrincipal.class));
    }

    @When("ingresa a la opcion login e ingresa {string} y {string}")
    public void ingresaALaOpcionLoginEIngresaY(String usuario, String pass) {
        when(theActorInTheSpotlight()).wasAbleTo(
                LoginWeb.conLasCredenciales(usuario,pass)
        );
    }

    @Then("el sistema le debe dejar entrar correctamente y ver mi {string}")
    public void elSistemaLeDebeDejarEntrarCorrectamenteYVerMi(String usuario) {
        then(theActorInTheSpotlight()).should(
                seeThat("Usuario Logueado",QuesGetText.getText(PaginaPrincipal.LABEL_USER), containsString(usuario))
        );
    }

    @When("da clic en categoria phones")
    public void daClicEnCategoriaPhones() {
        when(theActorInTheSpotlight()).wasAbleTo(
                ListarPhones.visualizar()
        );
    }

    @Then("el sistema le debe presentar la lista de phone")
    public void elSistemaLeDebePresentarLaListaDePhone() {
        then(theActorInTheSpotlight()).should(
                seeThat("Visualizar Samsung",QuesGetText.getText(PaginaProductos.LABEL_SAMSUNG), containsString("Samsung galaxy s6"))
        );
    }

    @When("da clic en Samsug galaxy s6")
    public void daClicEnSamsugGalaxyS6() {
        when(theActorInTheSpotlight()).wasAbleTo(
                ListarPhones.visualizar(),
                VerDescripcion.telefonos()
        );
    }

    @Then("el sistema le debe presentar el label Product description")
    public void elSistemaLeDebePresentarElLabelProductDescription() {
        then(theActorInTheSpotlight()).should(
                seeThat("Visualizar Descripcion telefonos",QuesGetText.getText(PaginaProductos.LABEL_DESCRIPCION), containsString("Product description"))
        );
    }

    @Given("que el {actor} ingresa a la lista de telefonos")
    public void queElClienteIngresaALaListaDeTelefonos(Actor actor) {
        givenThat(actor).attemptsTo(Open.browserOn().the(PaginaPrincipal.class));
    }

    @And("da clic en add to cart")
    public void daClicEnAddToCart() {
        andThat(theActorInTheSpotlight()).attemptsTo(
                AnadirCarro.producto()
        );
    }

    @Then("el sistema le debe presentar el modal con el mensaje Product added")
    public void elSistemaLeDebePresentarElModalConElMensajeProductAdded() {
    }

    @Then("el sistema le debe presentar el producto en el carrito de compras")
    public void elSistemaLeDebePresentarElProductoEnElCarritoDeCompras() {
        then(theActorInTheSpotlight()).should(
                seeThat("Visualizar Descripcion telefonos",QuesGetText.getText(CarritoCompra.LABEL_PRODUCTO),containsString("Samsung galaxy s6"))
        );
    }

    @And("da clic en ver carrito")
    public void daClicEnVerCarrito() {
        andThat(theActorInTheSpotlight()).attemptsTo(
                VisualizarCarrito.producto()
        );
    }

    @And("da clic en borrar producto")
    public void daClicEnBorarProducto() {
        and(theActorInTheSpotlight()).attemptsTo(
            EliminarProducto.carrito()
        );
    }

    @Then("el sistema debe permitir borrar el producto")
    public void elSistemaDebePermitirBorrarElProducto() {
    }
}