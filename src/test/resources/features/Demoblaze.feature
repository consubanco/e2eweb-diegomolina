@Demoblaze
Feature: Pruebas a la pagina de Demoblaze
  Con el objetivo de comprobar el conocimiento de automatizacion
  Como Consubanco
  Requiero realizar las valiaciones E2E de la pagina demoblaze

  @id:1 @RegistroWeb
  Scenario Outline: T-E2E-WEB-CA1- Registro en la web
    Given que el cliente ingresa a la pagina de demoblaze para  registrase
    When ingresa a la opcion sigup e ingresa "<usuario>" y "<password>"
    Then el sistema le debe dejar registrase correctamente
    Examples:
|usuario|password|
|diego1981|d45#528|
##      | @externaldata@datosUsuarios.csv |

  @id:2 @LoginWeb
  Scenario Outline: T-E2E-WEB-CA2- Login en la web
    Given que el cliente ingresa a la pagina de demoblaze para  comprar
    When ingresa a la opcion login e ingresa "<usuario>" y "<password>"
    Then el sistema le debe dejar entrar correctamente y ver mi "<usuario>"
    Examples:
|usuario|password|
|diego1981|d45#528|
##      | @externaldata@datosUsuarios.csv |

  @id:3 @ListaTelefonos
  Scenario: T-E2E-WEB-CA3- Lista de Telefonos
    Given que el cliente ingresa a la pagina de demoblaze para  comprar
    When da clic en categoria phones
    Then el sistema le debe presentar la lista de phone

  @id:4 @VisualizarProdcutDescription
  Scenario: T-E2E-WEB-CA4- Visualizar Descripcion del Producto
    Given que el cliente ingresa a la lista de telefonos
    When da clic en Samsug galaxy s6
    Then el sistema le debe presentar el label Product description

  @id:5 @VisualizarModal
  Scenario: T-E2E-WEB-CA5- Visualizar Modal Producto Adicionado
    Given que el cliente ingresa a la lista de telefonos
    When da clic en Samsug galaxy s6
    And da clic en add to cart
    Then el sistema le debe presentar el modal con el mensaje Product added

  @id:6 @VisualizarCarrito
  Scenario: T-E2E-WEB-CA6- Visualizar Carrito
    Given que el cliente ingresa a la lista de telefonos
    When da clic en Samsug galaxy s6
    And da clic en add to cart
    And da clic en ver carrito
    Then el sistema le debe presentar el producto en el carrito de compras

  @id:7 @EliminarProducto
  Scenario: T-E2E-WEB-CA7- Eliminmar Producto del Carrito
    Given que el cliente ingresa a la lista de telefonos
    When da clic en Samsug galaxy s6
    And da clic en add to cart
    And da clic en ver carrito
    And da clic en borrar producto
    Then el sistema debe permitir borrar el producto
